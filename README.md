# Spring-mvc lab

Le but de ce lab est une initialisation à Spring MVC en proposant une utilisation des grands principes.


Les requêtes http des exercices sont dans le fichier `tests.rest`


## Etape 0 : Principes de base
Le [design pattern MVC](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93controller) contient 3 acteurs: 
- model : la structure des données manipulées
- view : la manière de présenter les données
- controller : le lien entre demandes client, models et views.

Le controlleur Spring MVC répond sur le endpoint `/` et renvoi une vue texte `src/main/java/com/soprasteria/springmvclab/DefaultController.java`.
 
## Etape 1 : Création d'un nouveau service
Créer un nouveau `@Controller` spring nommé `HtmlController` retournant une vue html contenant `My TL` sur le endpoint `/quiestleboss`.

[exemple](https://spring.io/guides/gs/serving-web-content/)

<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/HtmlController.java`

  ```java
    package com.soprasteria.springmvclab;

    import org.springframework.stereotype.Controller;
    import org.springframework.web.bind.annotation.GetMapping;

    @Controller
    public class HtmlController {
        @GetMapping("/quiestleboss")
        public String getBoss(){
            return "boss";
        }
    }
  ```


  `src/main/resources/templates/boss.html`
  ```HTML
    <h1>My TL</h1>
  ```
</details>


## Etape 2 : Service avec un paramètre
Ajouter un `@RequestParam` nommé `force` qui permet de forcer la valeur dans le template html. Ce paramètre sera non `required` avec une `defaultValue` à `my TL`.

<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/HtmlController.java`

  ```java
    @GetMapping("/quiestleboss")
    public String getBoss(@RequestParam(name="force", required=false, defaultValue="my TL") String force, Model model){
        
		  model.addAttribute("theBoss", force);
      return "boss";
    }
  ```


  `src/main/resources/templates/boss.html`
  ```HTML
    <h1 th:text="${theBoss}"></h1>
  ```
</details>


## Etape 3 : Création d'un service GET Json
Pour qu'un service renvoi  du JSon, il faut simplement une view particulière renvoyant un modèle sous fomat json.

Créer un [`@RestController`](https://www.baeldung.com/spring-controller-vs-restcontroller#spring-mvc-rest-controller) nommé `JsonController` ayant une endpoint GET `/boss` renvoyant un objet `Boss` ayant `<3 My TL <3` comme `name`.


<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/JsonController.java`

  ```java
    package com.soprasteria.springmvclab;

    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RestController;

    @RestController
    public class JsonController {
        @GetMapping("/boss")
        public Boss getBoss(){
            return new Boss("<3 My TL <3");
        }
    }

  ```

</details>



## Etape 4 : Service json avec un paramètre
Modifier le endpoint `/boss` en endpoint `/boss/{name}` pour qu'il utilise la `@PathVariable` comme `name` de l'objet `Boss`.


<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/JsonController.java`

  ```java
    package com.soprasteria.springmvclab;

    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.PathVariable;
    import org.springframework.web.bind.annotation.RestController;

    @RestController
    public class JsonController {
        @GetMapping("/boss/{name}")
        public Boss getBoss(@PathVariable String name){
            return new Boss(name);
        }
    }
  ```

</details>

## Etape 5 : Création d'un service POST Json
Ajouter un endpoint POST `/boss` qui prend un objet `Boss` sauvegardant le `name` dans le contrôlleur.

<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/JsonController.java`

  ```java
    private String boss = "My TL <3";
    @PostMapping("/boss")
    public Boss setBoss(@RequestBody Boss boss){
        this.boss = boss.getName();
        return boss;
    }
  ```

</details>

## Etape 6 : Comprendre les endpoints : url
Ajouter un endpoint GET `/boss` qui renvoi un objet `Boss` qui contient la valeur sauvegarder à l'étape précédente.

<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/JsonController.java`

  ```java
    @GetMapping("/boss")
    public Boss getBoss(){
        return new Boss(this.boss);
    }
  ```

</details>

## Etape 7 : Comprendre les endpoints : consumes
Ajouter un endpoint GET `/boss` qui renvoi un objet `Boss` qui comme `name` la valeur `Sylvain` si le header `Content-Type` vaut `application/json`

<details>
  <summary>Réponse</summary>

  `src/main/java/com/soprasteria/springmvclab/JsonController.java`

  ```java    
    @GetMapping(value = "/boss", consumes = {"application/json"})
    public Boss getJsonBoss(){
       return  new Boss("Sylvain");
    }
  ```

</details>