package com.soprasteria.springmvclab;

public class Boss {
    private String name;

    public Boss(){

    }
    public Boss(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }

    public void setName(String name){
        this.name = name;
    }
}
